from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path('ask_password', views.ask_password, name="ask_password"),
    path("send", views.send, name="send"),
    path("thanks", views.thanks, name="thanks"),
]
