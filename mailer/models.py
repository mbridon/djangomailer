from django.db import models


class MailAccount(models.Model):
    email = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=20)


class SentMail(models.Model):
    sender = models.CharField(max_length=40)
    recipient = models.CharField(max_length=40)
    subject = models.CharField(max_length=80)
    text = models.CharField(max_length=600)
