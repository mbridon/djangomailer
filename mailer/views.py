from celery import Celery
from django.http import (HttpResponse, HttpResponseRedirect,
    HttpResponseNotFound)
from django.shortcuts import render
from django.core.mail import send_mail

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djangomailer.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from .forms import MailAccountForm, SendMailForm
from .models import MailAccount
from .tasks import your_async_task


def index(request):
    return HttpResponse('booyah!')


def ask_password(request):
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = MailAccountForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()

            return HttpResponseRedirect("/mailer/thanks")

    # if a GET (or any other method) we'll create a blank form
    else:
        form = MailAccountForm()

    return render(request, "mailer/mailaccount.html", {"form": form})


def thanks(request):
    return HttpResponse("thanks!")


app = Celery('tasks', backend='redis://localhost', broker='redis://localhost')


@app.task
def send(request):
    try:
        if request.method == 'POST':
            form = SendMailForm(request.POST)
            if form.is_valid():
                data = form.clean()
                form.save()
                email_auth_password = MailAccount.objects.filter(
                        email=data['sender']).first().password

                send_mail(
                    subject=data['subject'], from_email=data['sender'],
                    recipient_list=[data['recipient']], message=data['text'],
                    auth_password=email_auth_password, fail_silently=False)

                return HttpResponse("Email sent successfully 🎉")

            else:
                return HttpResponseNotFound(form.errors)

        else:
            form = SendMailForm()

        return render(request, "mailer/send.html", {"form": form})
    
    except Exception as e:
        raise e


def sent(request):
    return HttpResponse("Email sent successfully 🎉")
