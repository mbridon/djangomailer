from django import forms
from django.contrib.auth.models import User

from mailer.models import MailAccount, SentMail


class MailAccountForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = MailAccount
        fields = ('email', 'password')


class SendMailForm(forms.ModelForm):
    sender = forms.CharField(widget=forms.TextInput({"id": "from"}),
                             label="from", required=True)
    recipient = forms.EmailField(label="to", required=True)
    subject = forms.CharField(label='subject', required=True)
    text = forms.CharField(widget=forms.Textarea, label='text', required=True,
                           max_length=600)
    auth_password = MailAccount.objects.filter(email=sender).first()
    when = forms.DateTimeField(widget=forms.DateTimeInput(
        attrs={'type': 'datetime-local'}), required=True)

    class Meta:
        model = SentMail
        fields = ('sender', 'recipient', 'subject', 'text')
